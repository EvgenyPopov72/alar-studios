from alar_users import create_app

if __name__ == '__main__':
    create_app().run(port=8888, debug=True)
