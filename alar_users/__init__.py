import os

from flask import Flask, redirect, url_for
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app():
    root_path = os.path.join(os.getcwd(), 'alar_users')
    app = Flask('AlarUsers', root_path=root_path)
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SECRET_KEY'] = os.environ.get('SECRET_KEY', '5iqret')
    app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get(
        'DATABASE_URI', 'postgresql://alar:alar@localhost:5432/alar_users')

    db.init_app(app)
    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    from .models import User

    @login_manager.user_loader
    def load_user(user_uid):
        return User.query.get(user_uid)

    from alar_users.auth import auth
    app.register_blueprint(auth, url_prefix='/auth')
    from alar_users.users import users

    app.register_blueprint(users, url_prefix='/users')

    @app.route('/')
    def root_path():
        return redirect(url_for('auth.login'))

    return app
