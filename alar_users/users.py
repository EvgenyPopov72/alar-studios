from functools import wraps
from uuid import UUID

from flask import Blueprint, render_template, abort, jsonify, request
from flask_login import current_user, login_required

from alar_users import db
from alar_users.models import User

users = Blueprint('users', 'users')


def is_uuid(value):
    try:
        UUID(value)
    except ValueError:
        return False
    return True


def admin_required(func):
    """
    Проверяем текущего пользователя на наличие админских прав.
    """
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not current_user.is_admin:
            return abort(403)
        return func(*args, **kwargs)
    return decorated_view


@users.route('/')
@login_required
def index():
    db_users = db.session.execute(
        '''
        SELECT uid, email, name, is_admin
        FROM public.user
        ORDER BY is_admin DESC, name
        '''
    )
    return render_template(
        'index.html', users=db_users, current_user=current_user
    )


@users.route('/<uid>', methods=['DELETE'])
@login_required
@admin_required
def delete_user(uid):
    if not is_uuid:
        return abort(412)
    user = User.query.get(uid)
    if not user:
        return abort(404)
    if user == current_user:
        return abort(406)

    db.session.execute('DELETE FROM public.user WHERE uid=:uid', {'uid': uid})
    db.session.commit()

    return jsonify({"result": "success", "uid": uid, "method": "delete_user"})


@users.route('/<uid>', methods=['POST'])
@login_required
@admin_required
def edit_user(uid):
    params = request.get_json()
    if not params or not is_uuid(uid):
        return abort(412)

    user = User.query.get(uid)
    if not user:
        return abort(404)
    if user == current_user:
        return abort(406)

    db.session.execute(
        '''
        UPDATE
            public.user
        SET
            email=COALESCE(:email, email),
            name=COALESCE(:name, name),
            is_admin=COALESCE(:is_admin, is_admin)
        WHERE uid=:uid
        ''',
        dict(params, uid=uid)
    )
    db.session.commit()

    return jsonify({"result": "success", "uid": uid, "method": "edit_user"})
