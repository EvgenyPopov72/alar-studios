import uuid

from flask_login import UserMixin
from sqlalchemy.dialects.postgresql import UUID

from . import db


class User(UserMixin, db.Model):
    uid = db.Column(UUID(
        as_uuid=True), unique=True, primary_key=True, default=uuid.uuid4
    )
    email = db.Column(db.String(100), unique=True, nullable=False, index=True)
    hashed_password = db.Column(db.String(100), nullable=False)
    name = db.Column(db.String(100), nullable=False)
    is_admin = db.Column(db.Boolean, default=False)

    def get_id(self):
        return self.uid
