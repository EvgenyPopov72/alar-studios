from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import DataRequired, EqualTo


class LoginForm(FlaskForm):
    email = StringField(
        'Email',
        validators=[DataRequired()],
        render_kw={'placeholder': 'Email'}
    )
    password = PasswordField(
        'Password',
        validators=[DataRequired()],
        render_kw={'placeholder': 'Password'}
    )
    remember_me = BooleanField('Remember Me', default=False)


class SignupForm(FlaskForm):
    email = StringField(
        'Email',
        validators=[DataRequired()],
        render_kw={'placeholder': 'Email'}
    )
    name = StringField(
        'Name',
        validators=[DataRequired()],
        render_kw={'placeholder': 'User Name'}
    )
    password = PasswordField(
        'Password',
        validators=[
            DataRequired(),
            EqualTo('repeat_password', message='Passwords must match')],
        render_kw={'placeholder': 'Password'})
    repeat_password = PasswordField(
        'Repeat Password', render_kw={'placeholder': 'Repeat Password'}
    )
