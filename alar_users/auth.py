from functools import wraps
from urllib.parse import urlparse, urljoin

from flask import Blueprint, render_template, url_for, redirect, request, abort
from flask_login import login_required, logout_user, login_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash

from alar_users import db
from alar_users.forms import LoginForm, SignupForm
from alar_users.models import User

auth = Blueprint('auth', 'auth')


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    target_url = urlparse(urljoin(request.host_url, target))
    return target_url.scheme in (
        'http', 'https') and ref_url.netloc == target_url.netloc


def is_not_authenticated(func):
    """
    Проверяем на "незалогиненность" пользователя
    """
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if current_user.is_authenticated:
            return redirect(url_for('users.index'))
        return func(*args, **kwargs)
    return decorated_view


@auth.route('/login', methods=['GET', 'POST'])
@is_not_authenticated
def login():
    form = LoginForm()
    if not form.validate_on_submit():
        return render_template('login.html', form=form)

    email = form.email.data
    password = form.password.data
    remember_me = form.remember_me.data

    user = User.query.filter_by(email=email).first()
    if not user or not check_password_hash(user.hashed_password, password):
        # Не правильный пароль или имя пользователя
        # TODO сделать обработку этой ошибки на форме
        return abort(403)

    login_user(user, remember_me)
    next_url = request.args.get('next')
    if not is_safe_url(next_url):
        return abort(400)

    return redirect(next_url or url_for('users.index'))


@auth.route('/signup', methods=['GET', 'POST'])
@is_not_authenticated
def signup():
    form = SignupForm()
    if not form.validate_on_submit():
        return render_template('signup.html', form=form)

    email = form.email.data
    password = form.password.data
    name = form.name.data

    if User.query.filter_by(email=email).first():
        # Пользователь с таким email уже существуюет
        # TODO сделать обработку этой ошибки на форме
        return render_template('signup.html', form=form)

    password = generate_password_hash(password)

    user = User.query.first()
    # Первый пользователь в базе всегда создается c правами админа, остальные
    # создаются с не админскими правами
    is_admin = not user
    new_user = User(
        email=email, hashed_password=password, name=name, is_admin=is_admin
    )
    db.session.add(new_user)
    db.session.commit()
    # Сразу залогинимся новым пользователем
    login_user(new_user)
    return redirect(url_for('users.index'))


@auth.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))
