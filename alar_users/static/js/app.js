$("#update-user-btn" ).click(function() {
// Отправим запрос на бэкенд с новыми данными пользователя
  var modal = $("#editModal")
  var user_uid = $(modal).data('uid')
  var new_name = modal.find('.modal-body input#user-name').val()
  var new_email = modal.find('.modal-body input#user-email').val()
  var new_is_admin = modal.find('.modal-body input#user-is-admin').prop('checked')


  console.log( "Trying to update user:", user_uid);
    $.ajax({
    url: "/users/" + user_uid,
    type: "POST",
    contentType: "application/json; charset=utf-8",
    data: JSON.stringify({
        name: new_name,
        email: new_email,
        is_admin: new_is_admin
    })
  })
  .done(function(){
    console.log("Update success!")
  })
  .fail(function(){
    alert("Error while update user: " + user_uid);
  })
  .always(function(){
    $(modal).modal('hide')
    location.reload();
  });

});

$( "table.users button.delete" ).click(function() {
// Нажата кнопка удаления пользователя
  var row = $(this).parent().parent()
  var user_uid = row.data("uid")
  console.log( "Trying to delete user:", user_uid);

  $.ajax({
    url: "/users/" + user_uid,
    type: "DELETE",
  })
  .done(function(){
    row.remove()
  })
  .fail(function(){
    alert("Error while delete user: " + user_uid);
  });
});


$('#editModal').on('show.bs.modal', function (event) {
// Передадим параметры пользователя в модальное окно
  var button = $(event.relatedTarget)
  var row = $(button).parent().parent()
  var user_uid = row.data("uid")
  var email = row.find('.user-email').text()
  var name = row.find('.user-name').text()
  var is_admin = row.find('.user-is-admin').text() === 'True'

  var modal = $(this)
  $(modal).data('uid', user_uid)
  modal.find('.modal-body input#user-name').val(name)
  modal.find('.modal-body input#user-email').val(email)
  modal.find('.modal-body input#user-is-admin').prop('checked', is_admin)
})
